import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { makeNameTable } from './tableGenerator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  table = makeNameTable();
  elementCount: number;

  @ViewChild('tableRef', { read: ElementRef }) tableRef: ElementRef<HTMLElement>;

  ngAfterViewInit() {
    setTimeout(() => this.elementCount = this.tableRef.nativeElement.getElementsByTagName("*").length, 0);
  }
}
