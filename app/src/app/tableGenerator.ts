interface Row {
    cells: Array<string>;
}

export interface Table {
    headers: Array<string>;
    rows: Row[];
}

export function makeNameTable(): Table {
    return {
        headers: ['Name1', 'Name2', 'Name3', 'Name4', 'Name5'],
        rows: makeRows()
    }
}

function makeRows(): Row[] {
    const rows: Row[] = [];
    for (let x = 0; x < 500; x++) {
        const cells: string[] = [];
        for (let y = 0; y < 5; y++) {
            cells.push(getRandomName());
        }
        rows.push({cells});
    }
    return rows;
}

function getRandomName(): string {
    return names[Math.floor(Math.random() * Math.floor(names.length))];
}

const names = [
    'Beverly Ware',
    'Raymond Bennett',
    'Francesca Durham',
    'Randal Franklin',
    'Ronald Rosales',
    'Sean Henry',
    'Marquis Ramsey',
    'Judson Yu',
    'Forest Ho',
    'Melinda Fields',
    'Jefferey Martin',
    'Thad Lyons',
    'Valarie Guzman',
    'Manual Oliver',
    'Harry Sandoval',
    'Booker Vega',
    'Ethan Mcconnell',
    'Lamont Downs',
    'Terrence Juarez',
    'Zachary Dalton',
    'Sherrie Cox',
    'Alta Harmon',
    'Polly Kirby',
    'Seymour Manning',
    'Katheryn Stafford',
    'Abe Fernandez',
    'Reva Villegas',
    'Ezra Tyler',
    'Malcom Faulkner',
    'Eugenio Walton',
    'Claudine Hurst',
    'Osvaldo Roman',
    'Cruz Warren',
    'Cynthia Pacheco',
    'Elliott Strong',
    'Natalie Waters',
    'Francis Frazier',
    'Trey Marquez',
    'Jillian Lucas',
    'Morgan Lopez',
    'Luke Hendricks',
    'Terence Valentine',
    'Davis Aguirre',
    'Luz Keller',
    'Raphael Morse',
    'Paris Leblanc',
    'Jody Raymond',
    'Richard Kelly',
    'Kendall Mcknight',
    'Shana Becker'
];
